﻿using System;

namespace Ex9Homework
{
    class Program
    {
        static void Main(string[] args)
        {
           

            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\PR073493\Desktop\2Vectori.txt");
       
            System.Console.WriteLine("Vectors are:");

            foreach(string line in lines)
            {              
                Console.WriteLine( line);              
            }

            string[] v1=lines[0].Split(',');
            string[] v2 = lines[1].Split(',');

          

            int[] newV1 =new  int[v1.Length];
            int[] newV2 = new int[v2.Length];
            

            for (int i =0;i<v1.Length;i++)
                {                   
                    newV1[i] = Convert.ToInt32(v1[i].ToString());
                            
                }
            for (int i = 0; i < v2.Length; i++)
            {
                
                newV2[i] = Convert.ToInt32(v2[i].ToString());
            }

            int[] vect = Methods.CreateOneVector(newV1, newV2);
            System.Console.WriteLine("Final vector: ");

            foreach (int el in vect)
            {
                Console.WriteLine(el.ToString());
            }


        }
    }
}
