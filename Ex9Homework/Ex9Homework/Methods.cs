﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ex9Homework
{
    public class Methods
    {
        public static int[] Order(int[] sir)
        {
            int aux = 0;       
            for (int i = 0; i < sir.Length - 1; i++)
            {
                for (int j = i + 1; j < sir.Length; j++)
                {
                    if (sir[i] > sir[j])
                    {
                        aux = sir[i];
                        sir[i] = sir[j];
                        sir[j] = aux;
                    }
                }
            }
            return sir;
        }

        public static int[] CreateOneVector(int[] sir1, int[] sir2)
        {
            int[] newSir = new int[sir1.Length + sir2.Length];  
               
                for (int j = sir1.Length; j < sir1.Length + sir2.Length; j++)
                {
                for (int i = 0; i < sir1.Length; i++)
                {
                    newSir[i] = sir1[i];
                    
                }
                newSir[j] = sir2[j-sir1.Length];

            }                       
            return Order(newSir);
        }
    }
}
