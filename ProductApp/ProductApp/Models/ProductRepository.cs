﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductApp.Models
{
    
    public interface IProductRepository
    {
        IEnumerable<Product> Products { get; }
    }
    public class ProductRepository : IProductRepository
    {
        readonly List<Product> _products = new List<Product>
        {
            new Product
            {
                Id=1,
                Name="Milk",
                Description="Perfect for your breakfast"
            },

            new Product
            {
                Id=2,
                Name="Bread",
                Description="Made with whater and flour"
            }
        };

        public IEnumerable<Product> Products => _products;

        public Product GetById(int id)
        {
            return _products.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Product> GetAll() => _products;
    }
}
