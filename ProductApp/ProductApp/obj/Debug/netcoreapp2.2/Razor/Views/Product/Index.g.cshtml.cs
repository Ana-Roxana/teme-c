#pragma checksum "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\Product\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "faf103254ff258139575b9a62f6d6919911c1c70"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Product_Index), @"mvc.1.0.view", @"/Views/Product/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Product/Index.cshtml", typeof(AspNetCore.Views_Product_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\_ViewImports.cshtml"
using ProductApp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"faf103254ff258139575b9a62f6d6919911c1c70", @"/Views/Product/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b453d88089921cf47a1133bac97452c9b618d445", @"/Views/_ViewImports.cshtml")]
    public class Views_Product_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Product>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(31, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\Product\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
            BeginContext(74, 27, true);
            WriteLiteral("\r\n<h1>Index</h1>\r\n<p>\r\n    ");
            EndContext();
            BeginContext(102, 5, false);
#line 10 "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\Product\Index.cshtml"
Write(Model);

#line default
#line hidden
            EndContext();
            BeginContext(107, 11, true);
            WriteLiteral(";\r\n</p>\r\n\r\n");
            EndContext();
#line 13 "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\Product\Index.cshtml"
 foreach (var item in Model)
{

#line default
#line hidden
            BeginContext(151, 61, true);
            WriteLiteral("    <table>\r\n        <tr>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(213, 7, false);
#line 18 "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\Product\Index.cshtml"
           Write(item.Id);

#line default
#line hidden
            EndContext();
            BeginContext(220, 56, true);
            WriteLiteral(";\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(277, 9, false);
#line 21 "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\Product\Index.cshtml"
           Write(item.Name);

#line default
#line hidden
            EndContext();
            BeginContext(286, 56, true);
            WriteLiteral(";\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(343, 16, false);
#line 24 "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\Product\Index.cshtml"
           Write(item.Description);

#line default
#line hidden
            EndContext();
            BeginContext(359, 51, true);
            WriteLiteral(";\r\n            </td>\r\n        </tr>\r\n    </table>\r\n");
            EndContext();
#line 28 "C:\Users\PR073493\source\repos\ProductApp\ProductApp\Views\Product\Index.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Product>> Html { get; private set; }
    }
}
#pragma warning restore 1591
