﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ex8Homework.Models
{
    public class ProductRepository
    {
        readonly List<Product> _products = new List<Product>
        {
            new Product
            {
                Id=1,
                Name="Bread",
                Description="Made with whater and flour"
            },

            new Product
            {
                Id=2,
                Name="Whater",
                Description="Only for you"
            }
        };

        public List<Product> products => _products;

        public Product GetById(int id)
        {
            return _products.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Product> GetAll() => _products;
    }
}
