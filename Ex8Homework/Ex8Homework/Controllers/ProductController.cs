﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ex8Homework.Models;

namespace Ex8Homework.Controllers
{
    public class ProductController : Controller
    {
        readonly ProductRepository _repo;

        public ProductController(ProductRepository repo)
        {
            _repo = repo;
        }
        public IActionResult Index()
        {
            var product = _repo.products;
            return View(product);
        }
    }
}