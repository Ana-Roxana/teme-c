﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2Homework
{
    public class Animal
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public override string ToString() => $"Person info: {Id} {Name } {Description}";


    }
}
