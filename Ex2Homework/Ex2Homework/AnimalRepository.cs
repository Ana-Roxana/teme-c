﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2Homework
{

    public interface IAnimalRepository
    {
        void Add(Animal a);
        Animal EditAnimal(Animal a, string newName, string newDescription);
        void Delete(Animal a);
        IEnumerable<Animal> GetAll();


    }
    public class AnimalRepository:IAnimalRepository
    {
        readonly List<Animal> _animal = new List<Animal>();
        public void Add(Animal a)
        {
            _animal.Add(a);
        }

        public Animal EditAnimal(Animal a, string newName, string newDescription)
        {
            //Animal a = new Animal();
           
            a.Name = newName;
            a.Description = newDescription;
            return a;

        }

        public void Delete(Animal a)
        {
            var item = _animal.Find(x => x.Name == a.Name);
            _animal.Remove(item);
        }

        public IEnumerable<Animal> GetAll()
        {
            return _animal;
        }
    }
}
