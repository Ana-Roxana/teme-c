﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2Homework
{
    class Program
    {
        static void Main(string[] args)
        {

            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Dog",
                Description = "the perfect animal"
            };

            Animal a2 = new Animal
            {
                Id = 2,
                Name = "Cat",
                Description = "the perfect animal"
            };

            Animal a3 = new Animal
            {
                Id = 3,
                Name = "Horse",
                Description = "the perfect animal"
            };

            AnimalRepository repo = new AnimalRepository();

            repo.Add(a1);
            repo.Add(a2);
            repo.Add(a3);

            IEnumerable<Animal> animals = repo.GetAll();

            foreach(Animal a in animals)
            {
                Console.WriteLine(a);
            }

            repo.Delete(a3);

            foreach (Animal a in animals)
            {
                Console.WriteLine(a);
            }
            repo.Add(a3);
            repo.EditAnimal(a3, "Pig", "something about it");
            foreach (Animal a in animals)
            {
                Console.WriteLine(a);
            }
        }
    }
}
