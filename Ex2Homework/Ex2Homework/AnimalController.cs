﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2Homework
{
    public class AnimalController
    {
        readonly IAnimalRepository _animalRepository;

        public AnimalController(IAnimalRepository animalRepo)
        {
            _animalRepository = animalRepo;
        }

        public void Insert(Animal a)
        {
            _animalRepository.Add(a);

        }

        public Animal EditAnimal(Animal a, string name, string description)
        {
            return _animalRepository.EditAnimal(a, name, description);
        }

        public void Delete(Animal a)
        {
            _animalRepository.Delete(a);
        }

        public IEnumerable<Animal> GetAll() => _animalRepository.GetAll();
    }
}
