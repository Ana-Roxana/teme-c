﻿using System;

namespace Ex10Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");

            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\PR073493\Desktop\Vector.txt");

            System.Console.WriteLine("Vector is:");

            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }

            string[] v = lines[0].Split(',');
            int[] vect = new int[v.Length];

            for (int i = 0; i < v.Length; i++)
            {
                vect[i] = Convert.ToInt32(v[i].ToString());
            }

            foreach (int el in vect)
            {
                Console.WriteLine(el);
            }

            System.Console.WriteLine("New vector is:");

            foreach (int el in Sort(vect))
            {
                Console.WriteLine(el);
            }

        }

        public static int[] Sort(int[] sir)
        {
            int aux = 0;
            for(int i=0;i<sir.Length;i++)
            {
                if (sir[i] == 0)
                {
                    for (int j = i+1; j < sir.Length; j++) { 
                
                        sir[j-1] = sir[j];
                        
                    }
                    sir[sir.Length - 1] = aux;
                }
               
            }
            
            return sir;
        }
    }
}
