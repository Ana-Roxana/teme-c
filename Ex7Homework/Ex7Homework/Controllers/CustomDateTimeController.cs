﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ex7Homework.Models;

namespace Ex7Homework.Controllers
{
    public class CustomDateTimeController : Controller
    {

        readonly CustomDateTimeRepository _repo;

        public CustomDateTimeController(CustomDateTimeRepository repo)
        {
            _repo = repo;
        }

        public IActionResult Index()
        {
            var customDateTime = _repo.customDateTimes;
            
            return View(customDateTime);
        }
    }
}