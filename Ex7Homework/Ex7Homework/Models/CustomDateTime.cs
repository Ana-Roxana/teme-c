﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ex7Homework.Models
{
    public class CustomDateTime
    {
        public string Date { set; get; }
        public string Day { set; get; }
        public string Hour { set; get; }
    }
}
