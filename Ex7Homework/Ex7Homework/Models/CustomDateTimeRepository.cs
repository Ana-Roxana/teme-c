﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ex7Homework.Models
{
    public class CustomDateTimeRepository
    {
        readonly List<CustomDateTime> _dateTime = new List<CustomDateTime>
        {
            new CustomDateTime
            {
                Date="8.08.2019",
                Day="Thursday",
                Hour="10:10"                
            },
             new CustomDateTime
            {
                Date="9.08.2019",
                Day="Wednesday",
                Hour="11:10"
            }
        };

        public List<CustomDateTime> customDateTimes => _dateTime;
    }
}
