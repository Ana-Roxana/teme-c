﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ex3Homework
{
   public  class Person
    {
       public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Adress { set; get; }

        public override string ToString() => $"Person info: {FirstName} {LastName } {Adress}";
    }
}
