﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ex3Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>();

            Person p1 = new Person
            {
            FirstName="Popescu",
            LastName="Ana",
            Adress="Brasov,Romania",
            
            };
            Person p2 = new Person
            {
                FirstName = "Marin",
                LastName = "Alina",
                Adress = "Bucuresti,Romania",

            };
            Student s1 = new Student
            {
                FirstName = "Aldea",
                LastName = "Andrei",
                Adress = "Clij,Romania",
                medie = 9.21,
                nrMatricol = "100"

            };
            personList.Add(p1);
            personList.Add(p2);
            personList.Add(s1);

           foreach(Person p in personList)
            {
                Console.WriteLine(p);
            }

             List<Person> SortedList = personList.OrderBy(o => o.FirstName).ToList();
            

            foreach (Person p in SortedList)
            {
                Console.WriteLine(p);
            }

        
          
        }
    }
}
